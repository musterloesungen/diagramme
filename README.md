# Komponentendiagramm

```plantuml
@startuml
package "Linux System" {
    [Linux Namespace] as NS
    [Prozess] as PROC
    [File System] as FS
}

node "Docker" {
    [Docker Image] as IMG
    [Docker Container] as CONT
    [Image File System] as IMGFS
}

node "Kubernetes Cluster" {
    [Pod] as POD
    [Service] as SVC
    [Deployment] as DEP
    [StatefulSet] as STS
    [Job] as JOB
    [CronJob] as CRON
    [ReplicaSet] as RS
}

database "Konfiguration" {
    [Dockerfile]
    [Kubernetes Manifest] as MANIFEST
}

NS --> PROC : beinhaltet
PROC --> CONT : wird zu
IMG --> CONT : instanziiert
IMG ..> IMGFS : beinhaltet
CONT --> POD : deployed in
POD --> SVC : bietet
DEP --> POD : managed
RS --> POD : instanziiert
DEP ..> RS : verwendet
STS --> POD : managed
STS ..> RS : verwendet
JOB --> POD : führt aus
CRON --> JOB : plant
Dockerfile ..> IMG : baut
MANIFEST ..> DEP : konfiguriert
MANIFEST ..> STS : konfiguriert
MANIFEST ..> JOB : konfiguriert
MANIFEST ..> CRON : konfiguriert
FS ..> PROC : nutzt
@enduml
```

# Volumes

```mermaid

graph LR
    subgraph Pod
        PV["Pod-spezifische Volumes"]
        PV -->|Lebt mit Pod| ED[EmptyDir]
        PV -->|Konfigurationsdaten| CM[configMap]
        PV -->|Speichert Geheimnisse| S[secret]
        PV -->|Exponiert Pod-Infos| DA[downwardAPI]
    end

    subgraph Node
        NV["Node-zentrierte Volumes"]
        NV -->|Zugriff auf Host-Dateisystem| HP[hostPath]
        NV -->|Speichert lokale Daten| L[local]
    end

    subgraph Cluster
        CV["Cluster-weite Volumes"]
        CV -->|Netzwerk-Dateisystem| NFS[nfs]
        CV -->|Speichert dauerhafte Daten| PVC[persistentVolumeClaim]
        CV -->|Speicher-Interface| CSI[csi]
        CV -->|Verteiltes Dateisystem| CFS[cephfs]
        CV -->|Skalierbares Dateisystem| GF[glusterfs]
        CV -->|Blockspeicher über IP| ISCSI[iscsi]
        CV -->|Datenmigration unterstützen| F[flocker]
    end
```